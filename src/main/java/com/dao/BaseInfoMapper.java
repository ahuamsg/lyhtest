package com.dao;


import java.util.List;

import com.model.BaseInfo;
import com.model.User;

public interface BaseInfoMapper {
	
	public BaseInfo selectMap();
	
	public User selectByPrimaryKey(String Id);
	
	public void insertUser(User user);

	public int delete(BaseInfo info);

	public int update(BaseInfo info);

	public int insert(BaseInfo info);

	public Object selectOne(BaseInfo info);

	public List selectSingle(BaseInfo info);

	public List selectMap(BaseInfo info);
}
