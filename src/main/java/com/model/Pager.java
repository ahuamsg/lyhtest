package com.model;

import java.io.Serializable;

public class Pager implements Serializable {

	private Integer currentPage;
	private Integer totalItems;
	private Integer itemsPerPage;
	private Integer totalPages;

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(Integer totalItems) {
		this.totalItems = totalItems;
	}

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Pager() {
		super();
	}

	public Pager(Integer currentPage, Integer totalItems, Integer itemsPerPage, Integer totalPages) {
		super();
		this.currentPage = currentPage;
		this.totalItems = totalItems;
		this.itemsPerPage = itemsPerPage;
		this.totalPages = totalPages;
	}

	@Override
	public String toString() {
		return "Pager [currentPage=" + currentPage + ", totalItems=" + totalItems + ", itemsPerPage=" + itemsPerPage
				+ ", totalPages=" + totalPages + "]";
	}

}
