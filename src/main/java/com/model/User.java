package com.model;

import java.io.Serializable;

import com.suniot.entity.BaseEntity;
import com.suniot.entity.Page;

/**
 * @author lyh
 * @date 2015-06-25
 * @description 用户信息实体类
 *
 */
public class User extends BaseEntity implements Serializable{
	private static final long serialVersionUID = -8844765878066782694L;
	private int user_id;
	private String job_number;
	private String user_name;
	private String password;
	private String real_name;
	private String phone;
	private String email;
	private int status;
	private String last_login_time;
	private String last_login_ip;
	private int role_id;
	private String dept_id;
	private int[] dept_ids;
	private int wkpl_id;
	private int report_id;
	private String hire_date;
	private String add_time;
	private String remarks;
	private Page page;
	private String skin;
	private String operation;
	private int is_sync;
	private String ding_id;
	private String dimission_date;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getJob_number() {
		return job_number;
	}

	public void setJob_number(String job_number) {
		this.job_number = job_number;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLast_login_time() {
		return last_login_time;
	}

	public void setLast_login_time(String last_login_time) {
		this.last_login_time = last_login_time;
	}

	public String getLast_login_ip() {
		return last_login_ip;
	}

	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public int[] getDept_ids() {
		return dept_ids;
	}

	public void setDept_ids(int[] dept_ids) {
		this.dept_ids = dept_ids;
	}

	public int getWkpl_id() {
		return wkpl_id;
	}

	public void setWkpl_id(int wkpl_id) {
		this.wkpl_id = wkpl_id;
	}

	public int getReport_id() {
		return report_id;
	}

	public void setReport_id(int report_id) {
		this.report_id = report_id;
	}

	public String getHire_date() {
		return hire_date;
	}

	public void setHire_date(String hire_date) {
		this.hire_date = hire_date;
	}

	public String getAdd_time() {
		return add_time;
	}

	public void setAdd_time(String add_time) {
		this.add_time = add_time;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Page getPage() {
		if (page == null)
			page = new Page();
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public int getIs_sync() {
		return is_sync;
	}

	public void setIs_sync(int is_sync) {
		this.is_sync = is_sync;
	}

	public String getDing_id() {
		return ding_id;
	}

	public void setDing_id(String ding_id) {
		this.ding_id = ding_id;
	}

	public String getDimission_date() {
		return dimission_date;
	}

	public void setDimission_date(String dimission_date) {
		this.dimission_date = dimission_date;
	}

}
