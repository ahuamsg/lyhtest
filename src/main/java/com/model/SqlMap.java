package com.model;

import java.io.Serializable;

public class SqlMap implements Serializable {

	private String handle; // 句柄
	private Object param; // 参数

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public Object getParam() {
		return param;
	}

	public void setParam(Object param) {
		this.param = param;
	}

	public SqlMap() {
		super();
	}

	public SqlMap(String handle, Object param) {
		super();
		this.handle = handle;
		this.param = param;
	}

	@Override
	public String toString() {
		return "SqlMap [handle=" + handle + ", param=" + param + "]";
	}

}
