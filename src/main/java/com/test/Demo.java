package com.test;

/**
 * 这是一个类
 * */
public class Demo {   //这是类的定义  class 类名 这是类的开始
	private int a = 1; //成员变量又叫全局变量     定义
	private String str = "这是字符类型的成员变量又叫全局变量";
	
	/** 开始定义方法 
	 * 没有返回值
	 *   */
	public void test1(){
		System.out.println("这是test1方法,我可以得到成员变量或全局变量 -------------a:"+this.a+"-----str:"+this.str);
	}
	
	/** 结束该方法定义方法  */
	
	/** 开始定义方法 
	 * 有返回值 而且返回值类型为int
	 *   */
	public int test2(){
		int b = 3;
		System.out.println("这是test2方法，返回一个int类型的数据是："+b);
		return b;
	}
	
	/** 结束该方法定义方法  */
	
	
	/** 开始定义方法 
	 * 有返回值 而且返回值类型为String
	 *   */
	public String test3(){
	    String  b = "这是要返回的值的";
		System.out.println("这是test3方法，返回一个String类型的数据是:"+b);
		return b;
	}
	/** 结束该方法定义方法  */
	
	
	/** 开始定义方法 
	 * 无返回值 ，需要传一个参数
	 *   */
	public void  test4(int a){
		System.out.println("这是test4方法，需要给我传一个int类型的参数"+a);
	}
	/** 结束该方法定义方法  */
	
	/** 开始定义方法 
	 * 无返回值 ，需要传一个参数
	 *   */
	public void  test5(String a){
		System.out.println("这是test5方法，需要给我传一个String类型的参数"+a);
	}
	/** 结束该方法定义方法  */
	
	public static void main(String[] args){
		Demo demo = new Demo();   //这是实例化一个对象
		demo.test1();             //调用这个对象的一个方法
		int c = demo.test2();
		System.out.println("这是test2给我返回的数据："+c);
		String str = demo.test3();
		System.out.println("这是test3给我返回的数据："+str);
		demo.test4(5);
		demo.test5("aaaaaa");
	}
		
}//这是类的结束
